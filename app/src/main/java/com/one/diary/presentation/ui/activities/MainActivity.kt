package com.one.diary.presentation.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.one.diary.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        label_message.text = "Hello Kotlin!"
    }
}
